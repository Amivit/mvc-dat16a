using System;
using Xunit;

namespace MVC.Identity.Faelles2.Test
{
    using MVC.Identity.Faelles2.Services;

    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var fakeMathService = new FakeItEasy.Fake<MathService>().FakedObject;

            var output = fakeMathService.Sum(1, 2);

            // ARRANGE
            var mathService = new MathService();
            var input1 = 1;
            var input2 = 2;

            // ACT
            // var output = mathService.Sum(input1, input2);

            // ASSERT 
            Assert.Equal(output, input1 + input2);


        }
    }
}
